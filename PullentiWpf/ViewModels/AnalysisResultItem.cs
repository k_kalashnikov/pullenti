﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PullentiWpf.ViewModels
{
    public class AnalysisResultItem
    {
        public AnalysisResultItem()
        {
            Childs = new List<AnalysisResultItem>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<AnalysisResultItem> Childs { get; set; }
    }
}
