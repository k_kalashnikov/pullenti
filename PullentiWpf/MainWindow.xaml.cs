﻿using EP;
using Microsoft.Win32;
using PullentiWpf.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PullentiWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<AnalysisResultItem> AnalysisResult { get; set; }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void chouseFile_Click(object sender, RoutedEventArgs e)
        {
            resultText.Children.Clear();
            resultText.RowDefinitions.Clear();
            OpenFileDialog chouseFileDialog = new OpenFileDialog();
            if (chouseFileDialog.ShowDialog().Value)
            {
                using (StreamReader strR = new StreamReader(chouseFileDialog.OpenFile()))
                {
                    var fileText = strR.ReadToEnd();
                    textFromFile.Text = fileText;
                    var resultList = GetAnalysisResult(fileText);
                    if (resultList.Count() < 1)
                    {
                        MessageBox.Show($"Result list is empty");
                    }
                }
            }
        }

        public List<AnalysisResultItem> GetAnalysisResult(string sourceText)
        {
            List<AnalysisResultItem> result = new List<AnalysisResultItem>();
            Processor processor = new Processor();
            AnalysisResult rawResult = processor.Process(new SourceOfAnalysis(sourceText));
            result = rawResult.Entities.Where(m => m.InstanceOf.Name.Contains("PERSON")).Select(m =>
                new AnalysisResultItem()
                {
                    Name = m.InstanceOf.Name,
                    Description = m.ToString()
                }).ToList();
            result.ForEach(m =>
            {
                resultText.RowDefinitions.Add(new RowDefinition() {
                    MaxHeight = 50
                });
                Border tempName = new Border()
                {
                    MaxHeight = 50,
                    BorderBrush = Brushes.Black
                };
                tempName.Child = new TextBlock()
                {
                    Text = m.Name
                };
                resultText.Children.Add(tempName);
                Grid.SetColumn(tempName, 0);
                Grid.SetRow(tempName, resultText.RowDefinitions.Count() - 1);
                Border tempDesc = new Border()
                {
                    OverridesDefaultStyle = false,
                    MaxHeight = 50,
                    BorderBrush = Brushes.Black
                };
                tempDesc.Child = new TextBlock()
                {
                    Text = m.Description,
                    TextWrapping = TextWrapping.Wrap
                };
                resultText.Children.Add(tempDesc);
                Grid.SetColumn(tempDesc, 1);
                Grid.SetRow(tempDesc, resultText.RowDefinitions.Count() - 1);
            });
            return result;
        }
    }
}
